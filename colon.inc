%define NEXT_ELEMENT 0
%macro colon 2

    %ifid %2
        %2: dq NEXT_ELEMENT
        %define NEXT_ELEMENT %2
    %else
        %fatal "arg2: нужен идентификатор!"
    %endif

    %ifstr %1
        db %1, 0
    %else   
        %fatal "arg1: Ключом должен быть string!"
    %endif
%endmacro
