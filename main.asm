%include "words.inc"
%include "lib.inc"
%define BUFFER 256
%define DQ_SIZE 8
%define SYS_WRITE 1
%define FD_STDERR 2
extern find_word

global _start

section .data


not_found: db "Ключ нe найде!", 0
overflow: db "Буфeр переполнен!", 0
string_buffer: times BUFFER db 0

section .text

; Начнём с ввода строки в буфер
_start:
    xor rax, rax
    mov rdi, string_buffer
    mov rsi, BUFFER
    call read_word
    test rax, rax
    jne .success_read_buffer    ; Если будкт преполнение, то rax вернет 0 
    mov rdi, overflow
    call print_err
    call print_newline
    call exit

; 
.success_read_buffer:
    mov rdi, rax
    mov rsi, first
    push rdx                    ; Сохраняею длину строки ввода
    call find_word              

    test rax, rax               ; Если ключ нe был найдeн то rax = 0
    jne .success_key_found
    
    mov rdi, not_found          
    call print_err
    call print_newline
    call exit

.success_key_found:
    pop rdx                     ; Вернем длину строки
    add rax, 8                  ; Указать на следующий  элемент
    add rax, rdx                ; добавем длину 
    add rax, 1                  ; добавим 1 для нуль-термирование
    mov rdi, rax                ;   rax указывает на найденную строку
    call print_string
    call print_newline
    call exit

print_err:
    xor rax, rax

    mov rsi, rdi
    call string_length
    
    mov rdx, rax
    mov rdi, FD_STDERR

    mov rax, SYS_WRITE
    syscall 
    ret
