section .text
 
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global parse_uint
global parse_int
global string_equals
global string_copy
global read_char
global read_word
global exit

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    mov rdi, 0
    syscall
    ret 

end:
    ret


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
        .loop:
            cmp byte [rdi+rax],0
            je end
            inc rax
            jmp .loop
    ret
        
        

; Принимаeт указатель на нуль-тeрминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi            
    mov rsi, rsp        
    pop rdi             
    mov rax, 1          ; 'write' syscall number
    mov rdx, 1          
    mov rdi, 1          ; Указать на stdout
    syscall

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, 0xA
    push rsi
    mov rsi, rsp
    syscall
    pop rsi

    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
        xor rdx, rdx
        mov r8, 0             ; счётчик 
        mov r9, 10            ; делимоe, использованноe в функции
        mov rax, rdi          ; Взять аргумeнта в регистр аккумулятора


        .loop:                  
            div r9            ; Делить число на 10, для получение последнего число
            mov rsi, rdx      ; Остаток деление - последнее число. Его записать в rsi
            add rsi, 48       ; Перевод в ASCII
            inc r8            
            dec rsp           
            mov [rsp], sil    ; Записать остаток деление в стеке
            xor rdx, rdx      
            cmp rax, 0        ; Проверить последнее ли число
            jne .loop        
        mov rsi, rsp          ; Записать результат в rdi, чтобы передать в следующую функцию
        mov rax, 1
        mov rdx, r8
        mov rdi, 1
        syscall
        add rsp, r8           ; Вернуть стек в оргинальное состояние

    ret




; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
        xor rdx, rdx
        mov r8, 0
        mov r9, 10
        mov rax, rdi
        cmp rax, 0          
        jge .loop          
        .loop:
            div r9
            mov rsi, rdx
            add rsi, 48
            inc r8
            dec rsp
            mov [rsp], sil
            xor rdx, rdx
            test rax, rax
            jne .loop
        cmp rdi, 0
        jge .syscall
        inc r8
        dec rsp
        mov byte[rsp], '-'
        .syscall:
            mov rsi, rsp
            mov rax, 1
            mov rdx, r8
            mov rdi, 1
            syscall
            add rsp, r8

            ret
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    xor r9, r9
    mov rax, 1                
    mov r8, rdi
    mov r9, rsi
    xor rcx, rcx
    xor rdi, rdi
    xor rsi, rsi
    .loop: 
        mov dil, [r9+rcx]
        mov sil, [r8+rcx]
        cmp sil, dil
        jne .NotEqual
        cmp byte dil, 0
        je end
        inc rcx
        jmp .loop
    .NotEqual:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
        dec rsp
        mov rdi, 0          ; Указать на stdin
        mov rdx, 1          ; Указать на длину строки ввода
        mov rax, 0          ; 'read' syscall
        mov rsi, rsp
        syscall
        cmp rax, 0
        je .zero
        mov rax, [rsp]
        inc rsp
        ret 
        .zero:
            mov rax, 0
            inc rsp

            ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi

    .skip_whitespaces:          ; Цикл удаление предыдущих пробелов
        call read_char
        cmp al, ' '
        je .skip_whitespaces
        cmp al, 0x9
        je .skip_whitespaces
        cmp al, 0xA
    xor rdx, rdx               
    jmp .write


    .loop:                      ; Цикл ввода символа
        push rdx
        call read_char
        pop rdx
    .write:                      
        cmp al, 0xA            
        je .end
        cmp al, 0x20
        je .end
        cmp al, 4
        je .end 
        cmp al, 0x9
        je .end
        cmp al, 0
        je .end
        inc rdx
        cmp rdx, r9
        jge .overflow
        dec rdx
        mov [r8+rdx], al
        inc rdx
        jmp .loop
    .end:                       ; Сохраняем результат в rax если всё отлично
        mov byte[r8+rdx],0
        mov rax, r8
        ret
    .overflow:                  ; Вдруг слишком большой для буфера то прекращаем работу
        xor rax, rax



        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:


    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 10



    .loop: 
        mov r8b, [rdi + r9]
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end                     ; Проверить число ли аргумент
        sub r8b, '0'               

        mul r10                     ; Умножаем на основу системы счисления
        add rax, r8                 ; добавляем цифру

        inc r9                      
        jmp .loop
    .end:
        mov rdx, r9
        ret
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:

    cmp byte[rdi], '-'      ; Проверяем отрицательное ли число
    jne parse_uint          ; Если нет то просто идем к фукцию parse_uint
    inc rdi                 
    call parse_uint         ; Если отрицателен то парсим положительное число
    neg rax                 ; и сделаем его отрицательным
    cmp rdx, 0              
    je .end
    inc rdx                 ; Добавляем минус в длину строки
    .end:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    xor r9, r9
    call string_length              ; Узнаем длину строки
    cmp rax, rdx                    ; Вдруг длина строки меньше чем длины буфера
    jle .loop                       ; то идем в цикл
    mov rax, 0                      
    ret
    .loop:                          
        mov r9b, [rdi+rcx]
        mov [rsi +rcx],r9b
        inc rcx
        cmp byte[rsi + rcx],0
        jne .loop
    mov rax, rdx
    ret
